<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerInfoRepository")
 */
class CustomerInfo
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string",length=50, nullable=false)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $middle_name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $verification_id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $phone_no;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $email_id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\BoxInfo", mappedBy="customer_id", cascade={"persist", "remove"})
     */
    private $boxInfo;

    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getMiddleName(): ?string
    {
        return $this->middle_name;
    }

    public function setMiddleName(?string $middle_name): self
    {
        $this->middle_name = $middle_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(?string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getVerificationId(): ?string
    {
        return $this->verification_id;
    }

    public function setVerificationId(string $verification_id): self
    {
        $this->verification_id = $verification_id;

        return $this;
    }

    public function getPhoneNo(): ?string
    {
        return $this->phone_no;
    }

    public function setPhoneNo(string $phone_no): self
    {
        $this->phone_no = $phone_no;

        return $this;
    }

    public function getEmailId(): ?string
    {
        return $this->email_id;
    }

    public function setEmailId(?string $email_id): self
    {
        $this->email_id = $email_id;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getBoxInfo(): ?BoxInfo
    {
        return $this->boxInfo;
    }

    public function setBoxInfo(BoxInfo $boxInfo): self
    {
        $this->boxInfo = $boxInfo;

        // set the owning side of the relation if necessary
        if ($this !== $boxInfo->getCustomerId()) {
            $boxInfo->setCustomerId($this);
        }

        return $this;
    }
}
