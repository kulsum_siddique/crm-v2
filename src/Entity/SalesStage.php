<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SalesStageRepository")
 */
class SalesStage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $stage_name;

    public function getId()
    {
        return $this->id;
    }

    public function getStageName(): ?string
    {
        return $this->stage_name;
    }

    public function setStageName(string $stage_name): self
    {
        $this->stage_name = $stage_name;

        return $this;
    }
}
