<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AddressRepository")
 */
class Address
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $division;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $district;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $sub_district;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $thana;

    /**
     * @ORM\Column(type="integer")
     */
    private $post_code;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $union_or_ward;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $zone_name;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $area_name_or_locality;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $sector_no;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $road;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $block_no;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $house_no;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $flat_or_floor_no;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $remarks;

    public function getId()
    {
        return $this->id;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getDivision(): ?string
    {
        return $this->division;
    }

    public function setDivision(string $division): self
    {
        $this->division = $division;

        return $this;
    }

    public function getDistrict(): ?string
    {
        return $this->district;
    }

    public function setDistrict(string $district): self
    {
        $this->district = $district;

        return $this;
    }

    public function getSubDistrict(): ?string
    {
        return $this->sub_district;
    }

    public function setSubDistrict(?string $sub_district): self
    {
        $this->sub_district = $sub_district;

        return $this;
    }

    public function getThana(): ?string
    {
        return $this->thana;
    }

    public function setThana(string $thana): self
    {
        $this->thana = $thana;

        return $this;
    }

    public function getPostCode(): ?int
    {
        return $this->post_code;
    }

    public function setPostCode(int $post_code): self
    {
        $this->post_code = $post_code;

        return $this;
    }

    public function getUnionOrWard(): ?string
    {
        return $this->union_or_ward;
    }

    public function setUnionOrWard(?string $union_or_ward): self
    {
        $this->union_or_ward = $union_or_ward;

        return $this;
    }

    public function getZoneName(): ?string
    {
        return $this->zone_name;
    }

    public function setZoneName(string $zone_name): self
    {
        $this->zone_name = $zone_name;

        return $this;
    }

    public function getAreaNameOrLocality(): ?string
    {
        return $this->area_name_or_locality;
    }

    public function setAreaNameOrLocality(?string $area_name_or_locality): self
    {
        $this->area_name_or_locality = $area_name_or_locality;

        return $this;
    }

    public function getSectorNo(): ?string
    {
        return $this->sector_no;
    }

    public function setSectorNo(?string $sector_no): self
    {
        $this->sector_no = $sector_no;

        return $this;
    }

    public function getRoad(): ?string
    {
        return $this->road;
    }

    public function setRoad(?string $road): self
    {
        $this->road = $road;

        return $this;
    }

    public function getBlockNo(): ?string
    {
        return $this->block_no;
    }

    public function setBlockNo(?string $block_no): self
    {
        $this->block_no = $block_no;

        return $this;
    }

    public function getHouseNo(): ?string
    {
        return $this->house_no;
    }

    public function setHouseNo(?string $house_no): self
    {
        $this->house_no = $house_no;

        return $this;
    }

    public function getFlatOrFloorNo(): ?string
    {
        return $this->flat_or_floor_no;
    }

    public function setFlatOrFloorNo(?string $flat_or_floor_no): self
    {
        $this->flat_or_floor_no = $flat_or_floor_no;

        return $this;
    }

    public function getRemarks(): ?string
    {
        return $this->remarks;
    }

    public function setRemarks(?string $remarks): self
    {
        $this->remarks = $remarks;

        return $this;
    }
}
