<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FeedRepository")
 */
class Feed
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $feed_name;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $sharing_percentage;

    public function getId()
    {
        return $this->id;
    }

    public function getFeedName(): ?string
    {
        return $this->feed_name;
    }

    public function setFeedName(string $feed_name): self
    {
        $this->feed_name = $feed_name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSharingPercentage(): ?string
    {
        return $this->sharing_percentage;
    }

    public function setSharingPercentage(string $sharing_percentage): self
    {
        $this->sharing_percentage = $sharing_percentage;

        return $this;
    }
}
