<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BoxInfoRepository")
 */
class BoxInfo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $box_id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $box_nsc;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $box_mac;

    /**
     * @ORM\Column(type="integer")
     */
    private $previous_customer_id;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\BoxCategory", inversedBy="boxInfo", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="box_cat_id", referencedColumnName="box_cat_id",nullable=false)
     */
    private $box_cat_id;
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\BoxInfo", mappedBy="customer_id", cascade={"persist", "remove"})
     */

    private $customer_id;

    public function getId()
    {
        return $this->box_id;
    }


    public function setBoxId(int $box_id): self
    {
        $this->box_id = $box_id;

        return $this;
    }

    public function getBoxNsc(): ?string
    {
        return $this->box_nsc;
    }

    public function setBoxNsc(string $box_nsc): self
    {
        $this->box_nsc = $box_nsc;

        return $this;
    }

    public function getBoxMac(): ?string
    {
        return $this->box_mac;
    }

    public function setBoxMac(string $box_mac): self
    {
        $this->box_mac = $box_mac;

        return $this;
    }

    public function getPreviousCustomerId(): ?int
    {
        return $this->previous_customer_id;
    }

    public function setPreviousCustomerId(int $previous_customer_id): self
    {
        $this->previous_customer_id = $previous_customer_id;

        return $this;
    }

    public function getRelation(): ?BoxCategory
    {
        return $this->relation;
    }

    public function setRelation(BoxCategory $relation): self
    {
        $this->relation = $relation;

        return $this;
    }

    public function getBoxCatId(): ?BoxCategory
    {
        return $this->box_cat_id;
    }

    public function setBoxCatId(BoxCategory $box_cat_id): self
    {
        $this->box_cat_id = $box_cat_id;

        return $this;
    }

    public function getCustomerId(): ?CustomerInfo
    {
        return $this->customer_id;
    }

    public function setCustomerId(CustomerInfo $customer_id): self
    {
        $this->customer_id = $customer_id;

        return $this;
    }
}
