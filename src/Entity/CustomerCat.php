<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerCatRepository")
 */
class CustomerCat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $customer_cat_id;


    /**
     * @ORM\Column(type="string", length=100)
     */
    private $customer_cat_name;

    public function getCustomerCatId(): ?int
    {
        return $this->customer_cat_id;
    }

    public function setCustomerCatId(int $customer_cat_id): self
    {
        $this->customer_cat_id = $customer_cat_id;

        return $this;
    }

    public function getCustomerCatName(): ?string
    {
        return $this->customer_cat_name;
    }

    public function setCustomerCatName(string $customer_cat_name): self
    {
        $this->customer_cat_name = $customer_cat_name;

        return $this;
    }
}
