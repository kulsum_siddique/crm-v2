<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CablePackageRepository")
 */
class CablePackage
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string",length=50)
     */
    private $c_package_id;

    /**
     *@ORM\Column(type="string",length=50)
     */

    private $c_package_name;

    /**
     *@ORM\Column(type="integer")
     */

    private $c_package_mrc;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CablePackageCategory", inversedBy="c_package_id")
     * @ORM\JoinColumn(name="c_package_cat_id", referencedColumnName="c_package_cat_id",nullable=false)
     */
    private $c_package_cat;

    public function getId()
    {
        return $this->c_package_id;
    }

    public function getCPackageCat(): ?CablePackageCategory
    {
        return $this->c_package_cat;
    }

    public function setCPackageCat(?CablePackageCategory $c_package_cat): self
    {
        $this->c_package_cat = $c_package_cat;

        return $this;
    }
}
