<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CablePackageCategoryRepository")
 */
class CablePackageCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $c_package_cat_id;

    /**
     *@ORM\Column(type="string",length=50)
     */

    private $c_package_cat_name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CablePackage", mappedBy="c_package_cat_id")
     */
    private $c_package_id;

    public function __construct()
    {
        $this->c_package_id = new ArrayCollection();
    }

    public function getId()
    {
        return $this->c_package_cat_id;
    }

    /**
     * @return Collection|CablePackage[]
     */
    public function getCPackageId(): Collection
    {
        return $this->c_package_id;
    }

    public function addCPackageId(CablePackage $cPackageId): self
    {
        if (!$this->c_package_id->contains($cPackageId)) {
            $this->c_package_id[] = $cPackageId;
            $cPackageId->setCPackageCat($this);
        }

        return $this;
    }

    public function removeCPackageId(CablePackage $cPackageId): self
    {
        if ($this->c_package_id->contains($cPackageId)) {
            $this->c_package_id->removeElement($cPackageId);
            // set the owning side to null (unless already changed)
            if ($cPackageId->getCPackageCat() === $this) {
                $cPackageId->setCPackageCat(null);
            }
        }

        return $this;
    }
}
