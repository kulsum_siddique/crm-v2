<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BoxCategoryRepository")
 */
class BoxCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $box_cat_id;


    /**
     *@ORM\Column(type="string",length=50)
     */

    private $box_cat_name;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\BoxInfo", mappedBy="box_cat_id", cascade={"persist", "remove"})
     */
    private $boxInfo;

    public function getId()
    {
        return $this->box_cat_id;
    }

    public function getBoxInfo(): ?BoxInfo
    {
        return $this->boxInfo;
    }

    public function setBoxInfo(BoxInfo $boxInfo): self
    {
        $this->boxInfo = $boxInfo;

        // set the owning side of the relation if necessary
        if ($this !== $boxInfo->getBoxCatId()) {
            $boxInfo->setBoxCatId($this);
        }

        return $this;
    }
}
