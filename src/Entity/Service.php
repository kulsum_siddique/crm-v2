<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ServiceRepository")
 */
class Service
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $service_id;

    /**
    *@ORM\Column(type="string",length=50)
     */

    private $service_cat_name;

    /**
     *@ORM\Column(type="string",length=100)
     */

    private $description;

    /**
     *@ORM\Column(type="text")
     */

    private $remarks;

    public function getId()
    {
        return $this->id;
    }

    public function getServiceId(): ?int
    {
        return $this->service_id;
    }

    public function setServiceId(int $service_id): self
    {
        $this->service_id = $service_id;

        return $this;
    }

    public function getServiceCatName(): ?string
    {
        return $this->service_cat_name;
    }

    public function setServiceCatName(string $service_cat_name): self
    {
        $this->service_cat_name = $service_cat_name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRemarks(): ?string
    {
        return $this->remarks;
    }

    public function setRemarks(string $remarks): self
    {
        $this->remarks = $remarks;

        return $this;
    }
}
