<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InternetPackageRepository")
 */
class InternetPackageCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $internet_package_cat_id;

    /**
     *@ORM\Column(type="string",length=50)
     */

    private $internet_package_cat_name;

    public function getId()
    {
        return $this->internet_package_cat_id;
    }
}
