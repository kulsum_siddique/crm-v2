<?php

namespace App\Repository;

use App\Entity\CablePackage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CablePackage|null find($id, $lockMode = null, $lockVersion = null)
 * @method CablePackage|null findOneBy(array $criteria, array $orderBy = null)
 * @method CablePackage[]    findAll()
 * @method CablePackage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CablePackageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CablePackage::class);
    }

//    /**
//     * @return CablePackage[] Returns an array of CablePackage objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CablePackage
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
