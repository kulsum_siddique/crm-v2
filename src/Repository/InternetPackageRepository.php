<?php

namespace App\Repository;

use App\Entity\InternetPackage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InternetPackage|null find($id, $lockMode = null, $lockVersion = null)
 * @method InternetPackage|null findOneBy(array $criteria, array $orderBy = null)
 * @method InternetPackage[]    findAll()
 * @method InternetPackage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InternetPackageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InternetPackage::class);
    }

//    /**
//     * @return InternetPackage[] Returns an array of InternetPackage objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InternetPackage
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
