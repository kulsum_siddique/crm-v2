<?php

namespace App\Repository;

use App\Entity\BoxCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BoxCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method BoxCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method BoxCategory[]    findAll()
 * @method BoxCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BoxCategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BoxCategory::class);
    }

//    /**
//     * @return BoxCategory[] Returns an array of BoxCategory objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BoxCategory
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
