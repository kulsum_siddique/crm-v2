<?php

namespace App\Repository;

use App\Entity\CustomerCat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CustomerCat|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerCat|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerCat[]    findAll()
 * @method CustomerCat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerCatRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CustomerCat::class);
    }

//    /**
//     * @return CustomerCat[] Returns an array of CustomerCat objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CustomerCat
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
