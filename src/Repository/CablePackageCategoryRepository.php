<?php

namespace App\Repository;

use App\Entity\CablePackageCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CablePackageCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method CablePackageCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method CablePackageCategory[]    findAll()
 * @method CablePackageCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CablePackageCategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CablePackageCategory::class);
    }

//    /**
//     * @return CablePackageCategory[] Returns an array of CablePackageCategory objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CablePackageCategory
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
