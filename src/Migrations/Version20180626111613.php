<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180626111613 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cable_package ADD c_package_cat_id INT NOT NULL');
        $this->addSql('ALTER TABLE cable_package ADD CONSTRAINT FK_152F86182FCEF6D7 FOREIGN KEY (c_package_cat_id) REFERENCES cable_package_category (c_package_cat_id)');
        $this->addSql('CREATE INDEX IDX_152F86182FCEF6D7 ON cable_package (c_package_cat_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cable_package DROP FOREIGN KEY FK_152F86182FCEF6D7');
        $this->addSql('DROP INDEX IDX_152F86182FCEF6D7 ON cable_package');
        $this->addSql('ALTER TABLE cable_package DROP c_package_cat_id');
    }
}
