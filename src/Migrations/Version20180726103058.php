<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180726103058 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE box_info (box_id INT AUTO_INCREMENT NOT NULL, box_cat_id INT NOT NULL, customer_id  VARCHAR(50) NOT NULL, box_nsc VARCHAR(50) NOT NULL, box_mac VARCHAR(50) NOT NULL, previous_customer_id INT NOT NULL, UNIQUE INDEX UNIQ_3DED7D5E4805E9D5 (box_cat_id), UNIQUE INDEX UNIQ_3DED7D5E9395C3F3 (customer_id), PRIMARY KEY(box_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE box_info ADD CONSTRAINT FK_3DED7D5E4805E9D5 FOREIGN KEY (box_cat_id) REFERENCES box_category (box_cat_id)');
        $this->addSql('ALTER TABLE box_info ADD CONSTRAINT FK_3DED7D5E9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer_info (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE box_info');
    }
}
