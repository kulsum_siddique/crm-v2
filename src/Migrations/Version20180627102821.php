<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180627102821 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, country VARCHAR(100) NOT NULL, division VARCHAR(80) NOT NULL, district VARCHAR(80) NOT NULL, sub_district VARCHAR(80) DEFAULT NULL, thana VARCHAR(80) NOT NULL, post_code VARCHAR(255) NOT NULL, union_or_ward INT DEFAULT NULL, zone_name VARCHAR(80) NOT NULL, area_name_or_locality VARCHAR(80) DEFAULT NULL, sector_no VARCHAR(50) DEFAULT NULL, road VARCHAR(50) DEFAULT NULL, block_no VARCHAR(20) DEFAULT NULL, house_no VARCHAR(20) DEFAULT NULL, flat_or_floor_no VARCHAR(20) DEFAULT NULL, remarks VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE address');
    }
}
