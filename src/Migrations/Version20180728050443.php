<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180728050443 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE box_info DROP FOREIGN KEY FK_3DED7D5E9395C3F3');
        $this->addSql('DROP INDEX UNIQ_3DED7D5E9395C3F3 ON box_info');
        $this->addSql('ALTER TABLE box_info ADD id VARCHAR(50) NOT NULL, DROP customer_id');
        $this->addSql('ALTER TABLE box_info ADD CONSTRAINT FK_3DED7D5EBF396750 FOREIGN KEY (id) REFERENCES customer_info (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3DED7D5EBF396750 ON box_info (id)');
        $this->addSql('ALTER TABLE customer_info CHANGE id id VARCHAR(50) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE box_info DROP FOREIGN KEY FK_3DED7D5EBF396750');
        $this->addSql('DROP INDEX UNIQ_3DED7D5EBF396750 ON box_info');
        $this->addSql('ALTER TABLE box_info ADD customer_id INT NOT NULL, DROP id');
        $this->addSql('ALTER TABLE box_info ADD CONSTRAINT FK_3DED7D5E9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer_info (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3DED7D5E9395C3F3 ON box_info (customer_id)');
        $this->addSql('ALTER TABLE customer_info CHANGE id id INT AUTO_INCREMENT NOT NULL');
    }
}
